const request = require('supertest')
const socketClient = require('socket.io-client')
const server = require('./app')

describe('node app', () => {
  let client

  afterAll((done) => {
    server.close(done)
  })

  test('should serve static files', async () => {
    const res = await request(server).get('/')
    expect(res.status).toBe(200)
  })

  test('should handle socket connection and stats emission', (done) => {
    client = socketClient.connect('http://localhost:3000')

    client.on('connect', () => {
      console.log('connected');
    })

    client.on('stats', (data) => {
      expect(data).toHaveProperty('cpu');
      expect(data).toHaveProperty('ram');
      client.disconnect();
      done();
    })
  })

  test('should handle socket disconnection', (done) => {
    client = socketClient.connect('http://localhost:3000');

    client.on('connect', () => {
      client.disconnect()
    });

    client.on('disconnect', () => {
      console.log('disconnected')
      done()
    })
  })
})