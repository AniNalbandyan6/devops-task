# CPU/RAM monitoring with Node.js



## Description

This project involves developing a Node.js application to monitor and display real-time CPU and RAM usage. It includes setting up the application on a Kubernetes cluster, implementing static code analysis and code coverage tools, and establishing a CI/CD pipeline for continuous integration and deployment.

## How to run

To run the project open a command line, navigate to the project directory and run 

```node app.js```

This should display the port on which the app runs which is localhost:3000