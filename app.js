const express = require('express')
const http = require('http')
const socket = require('socket.io')
const os = require('os')

const app = express()
const server = http.createServer(app)
const web = socket(server)

app.use(express.static('public'))

web.on('connection', (socket)=>{
    console.log("new connection")

    setInterval(()=>{
        const total = os.totalmem()
        const free = os.freemem()
        const used = total-free
        const memoryUsage = (used/total)*100

        const cpus = os.cpus()
        const cpu = cpus.map(cpu=>{
            const times = cpu.times
            const idle = times.idle
            const totalCpu = Object.values(times).reduce((acc, tv)=>acc+tv, 0)
            return 100-((idle/totalCpu)*100)
        }).reduce((acc, load) => acc + load, 0) / cpus.length

        socket.emit('stats', {
           cpu: cpu,
           ram: memoryUsage 
        })
    }, 500)
    socket.on('disconnect', ()=>{
        console.log('disconnect')
    })
})
const port = 3000
server.listen(port, ()=>console.log(`running on port ${port}`))
module.exports = server